__all__ = ['main']

import os
from argparse import ArgumentParser
import Simfind

default_json_file = '/afs/slac.stanford.edu/u/ki/yymao/bin/_simfind_default.json'

def main():
    parser = ArgumentParser(description='simfind %s by %s'%(\
            Simfind.__version__, Simfind.__author__))
    parser.add_argument('-c', action='store_true', \
            help='Disable the interactive prompts.')
    parser.add_argument('-D', action='store_true',\
            help='Disable (skip) the default json file: \'%s\'.'%(\
            default_json_file))
    parser.add_argument('-f', metavar='PATH', default='~/.simfind', \
            help='Specify one json file (or one directory which has *.json files).')
    parser.add_argument('SIM_NAMES', nargs='*', help='Look up SIM_NAMES')
    parser.add_argument('--json-paths', metavar='PATH', nargs='+', default=[],\
            help='Specify more json files (or directories.) This option needs to follow after the SIM_NAMES.')
    args = parser.parse_args()

    json_paths = []
    if not args.D:
        json_paths.append(default_json_file)
    
    for p in [args.f] + args.json_paths:
        p = os.path.expanduser(p)
        if os.path.isdir(p):
            json_paths.extend(map(lambda x: '%s/%s'%(p, x), \
                    filter(lambda x: x.endswith('.json'), os.listdir(p))))
        elif os.path.isfile(p):
            json_paths.append(p)
        else:
            print 'Warning: %s is skipped because the path does not exist.'%(p)
    
    s = Simfind.Simfind()
    for p in json_paths:
        try:
            s.addJson(p, reindex=False)
        except IOError:
            print 'Warning: %s is skipped because it cannot be accessed.'%(p)
        except ValueError:
            print 'Warning: %s is skipped because it is not in a correct json format.'%(p)
    
    if len(s.data) == 0:
        parser.error(message="Error: No valid json file is found.\n")
    s.indexOptions()
    s.interactive_prompt = not args.c

    if len(args.SIM_NAMES) == 0:
        s.promptInteractive()
    else:
        s.queryInteractive(args.SIM_NAMES)
    

if __name__ == "__main__":
    main()
